import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;//������������ ��� ���
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
 
public class MainWindow extends Application {
 
    private Scene scene;
    double width = 700;
    double height = 500;
    
    @Override public void start(Stage stage) {
    	// create scene
        stage.setTitle("�������������� ������ � ������ ����������");
        scene = new Scene(new Browser(),width,height, Color.web("#666970"));
        stage.setScene(scene);
        // show stage
        stage.show();                
    }
 
    public static void main(String[] args){
    	launch(args); 	
    }
}
class Browser extends Region {
   private VBox toolBar;
 
   private static String[] headLabel = new String[]{
    	"������",
    	"������������ �������"
    };
   
    private static String[] captionsLect = new String[]{
        "������ 1 ��������",
        "������ 2 ���������. �������� ��� �����������",
        "������ 3 ��������� ������������. �������� ���������",
        "������ 4 ��������� �� ����������. �������� �������� ���������",
        "������ 5 ������������ � �������� ��� ��������������",
	        "������ 1 ��������",
	        "������ 2 ���������. �������� ��� �����������",
	        "������ 3 ��������� ������������. �������� ���������",
	        "������ 4 ��������� �� ����������. �������� �������� ���������",
	        "������ 5 ������������ � �������� ��� ��������������",		      
    };
    
    private static String[] captionsPrac = new String[]{
    	"�� 1 ��������� � �������� ��� ����",
    	"�� 2 �������������� ��������",
    	"�� 3 ��������� �� ����������",
    	"�� 4 ������ ������������",
    	"�� 5 ������������ ���������� �������",
	    	"�� 1 ��������� � �������� ��� ����",
	    	"�� 2 �������������� ��������",
	    	"�� 3 ��������� �� ����������",
	    	"�� 4 ������ ������������",
	    	"�� 5 ������������ ���������� �������"
    };
    
    private static String[] captionsExampleCalc = new String[]{
    	"������ 1",
    	"������ 2",
    	"������ 3",
    	"������ 4",
    	"������ 5",
	    	"������ 1",
	    	"������ 2",
	    	"������ 3",
	    	"������ 4",
	    	"������ 5"
    };    
    
 
    private static String[] urlsLect = new String[]{
    	"\\Lecture\\"+"������%201%20��������.htm",
    	"\\Lecture\\"+"������%202%20���������.%20��������%20���%20�����������.htm",
    	"\\Lecture\\"+"������%203%20���������%20������������.%20��������%20���������.htm",
    	"\\Lecture\\"+"������%204%20���������%20��%20����������.%20��������%20��������%20���������.htm",
    	"\\Lecture\\"+"������%205%20������������%20�%20��������%20���%20��������������.htm",
    			"\\Lecture\\"+"������%201%20��������.htm",
				"\\Lecture\\"+"������%202%20���������.%20��������%20���%20�����������.htm",
		    	"\\Lecture\\"+"������%203%20���������%20������������.%20��������%20���������.htm",
		    	"\\Lecture\\"+"������%204%20���������%20��%20����������.%20��������%20��������%20���������.htm",
		    	"\\Lecture\\"+"������%205%20������������%20�%20��������%20���%20��������������.htm"				    	
    };
    
    private static String[] urlsPrac = new String[]{
    	"\\Practic\\"+"��%201%20���������%20�%20��������%20���%20����..htm",
    	"\\Practic\\"+"��%202%20��������������%20��������.htm",
    	"\\Practic\\"+"��%203%20���������%20��%20����������.htm",
    	"\\Practic\\"+"��%204%20������%20������������.htm",
    	"\\Practic\\"+"��%205%20������������%20����������%20�������.htm",
    			"\\Practic\\"+"��%201%20���������%20�%20��������%20���%20����..htm",
    	    	"\\Practic\\"+"��%202%20��������������%20��������.htm",
    	    	"\\Practic\\"+"��%203%20���������%20��%20����������.htm",
    	    	"\\Practic\\"+"��%204%20������%20������������.htm",
    	    	"\\Practic\\"+"��%205%20������������%20����������%20�������.htm"
    };
    
    //������� ������� ��� �����������
    final ImageView selectedImage = new ImageView();    
    
    //������ ������ �� ������
    final Hyperlink[] hplsLect = new Hyperlink[captionsLect.length];
    
    //������ ������ �� ������������ �������
    final Hyperlink[] hplsPrac = new Hyperlink[captionsPrac.length];
    
    //������ ������ �� ���������� �������� ������������ ������
    final Hyperlink[] hplsExampleCalc = new Hyperlink[captionsExampleCalc.length];    
    
    //������ ����������
    final Text[] textHead = new Text[headLabel.length];
            
    //��� �������
    final WebView browser = new WebView();
    
    //������ ��������    
    final WebEngine webEngine = browser.getEngine(); 
   
    ScrollPane sp;
    public Browser() {       
    	
    	//apply the styles
        getStyleClass().add("browser");       
        //����������� ���������
        for (int i = 0; i < headLabel.length; i++){ 
        	textHead[i] = new Text(headLabel[i]);
        	textHead[i].setFont(Font.font("Tahoma", FontWeight.NORMAL, 16));
        }        
        //����������� ������ �� ������
        for (int i = 0; i < captionsLect.length; i++){            
        	//������ �������� �� ������� ����������� ����������� ������ �� �������� �� ������� �����, ����� ��� ������������� ������� ������
        	final Hyperlink hpl = hplsLect[i] = new Hyperlink(captionsLect[i]);            
            //������
            final String url = invertPath(urlsLect[i]); 
            hpl.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                	webEngine.load(url);   
                }
            });
        }
        
	   //����������� ������ �� ������������ �������      
       for (int i = 0; i < captionsPrac.length; i++){            
        	//������ �������� �� ������� ����������� ����������� ������ �� �������� �� ������� �����, ����� ��� ������������� ������� ������
        	final Hyperlink hpl = hplsPrac[i] = new Hyperlink(captionsPrac[i]);            
            //������
            final String url = invertPath(urlsPrac[i]); 
            hpl.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    webEngine.load(url); 
                    
                }
            });           
        }
       
       //����������� ������ �� ����� ����
       for (int i = 0; i < captionsExampleCalc.length; i++){            
       final Hyperlink hpl = hplsExampleCalc[i] = new Hyperlink(captionsExampleCalc[i]);            
       final String labelWindow = captionsExampleCalc[i];
       final int id = i;
       //������           
           hpl.setOnAction(new EventHandler<ActionEvent>() {
               @Override
               public void handle(ActionEvent e) {
                   //webEngine.load(url);
                   Example.createExample(labelWindow, id);
               }
           });
       }      

       // load the home page        
       webEngine.load(invertPath(urlsLect[0]));       
        
        // create the toolbar
        toolBar = new VBox();
        toolBar.getStyleClass().add("browser-toolbar");
        toolBar.getChildren().add(textHead[0]);
        toolBar.getChildren().addAll(hplsLect);        
        toolBar.getChildren().add(textHead[1]);
        
        int j=0;
        for(int i=0; i < hplsPrac.length; i++){
	        toolBar.getChildren().add(hplsPrac[i]);
	        toolBar.getChildren().add(hplsExampleCalc[j]);
	        if( j< hplsExampleCalc.length) j++;
        }
        
        //�������� ����������� �������
        createSpacer();        
        
        sp = new ScrollPane();//������ � ������������ ���������       
        sp.setCursor(Cursor.CLOSED_HAND);//������ � ���� ����              
        sp.setStyle("-fx-border-width:1pt;-fx-border-color:darkgray;");//������ ����� �����       
        sp.setTooltip(new Tooltip("����������� ��������� �������)))"));       
        sp.setContent(toolBar);
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);//�������������� ������ ���������
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);//ALWAYS - ������, AS_NEEDED - �� ������������� 
        sp.setPannable(true);//��������� ������ ������ ����������
        
        Group gr = new Group();//����� �������� ������ ����� ���� ����� ���� �� �������
        gr.getChildren().add(sp); 
        
        //add components
        getChildren().add(gr);
        getChildren().add(browser);                
    }
 
    public static String invertPath(String path){
    	path = "file:///" + System.getProperty("user.dir") + path;
    	String newString = "";
    	for(int i=0; i<path.length(); i++){
    		final Character ch = path.charAt(i);    		
    		if (ch == '\\'){
    			newString+="/";
    		}
    		else 
    			newString+=String.valueOf(ch);    		
    	}		
    	return newString;
	}	
    
    private Node createSpacer() {
        Region spacer = new Region();
        VBox.setVgrow(spacer, Priority.ALWAYS);
        return spacer;
    }
 
    @Override protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        sp.setPrefSize(w/3, h);
        layoutInArea(browser, w/3 , 0 , w-w/3 , h , 0, HPos.CENTER, VPos.CENTER);
        layoutInArea(toolBar,  0  , 0 , w/2   , h , 0, HPos.CENTER, VPos.CENTER);
    }  
 
    @Override protected double computePrefWidth(double height) {
        return 750;
    }
 
    @Override protected double computePrefHeight(double width) {
        return 500;
    }
}