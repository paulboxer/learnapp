import ExampleCalc.DemoAmountTwoValue;
import ExampleCalc.DemoSquareValue;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
 
public class Example{   
    
    public static void createExample(String title, int id){
		Stage window = new Stage();
		
		//Block events to other windows
		window.initModality(Modality.APPLICATION_MODAL);//��� ����������� ������ ������ �� ��� ������
		window.setTitle(title);
		window.setMinWidth(250);				
		
		//Display window and wait for it to be closed before returning
		Scene scene = null; 
		
		boolean lock = true;
		
		switch(id){
		    case 0: 
		    	scene = new DemoAmountTwoValue().getScene();
				break;
			case 1: 
				scene = new DemoSquareValue().getScene();
				break;		
			default: 
			    System.out.println("����� � �� " + id + " �� ����������");
			    lock = false;
			    break;
	    }		
		
		if (lock){
			window.setScene(scene);
			window.showAndWait(); //��������� ��� ���������� �� ������ show()		
		}
		
	}
    
    
    
}
