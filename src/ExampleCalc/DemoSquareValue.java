package ExampleCalc;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;//���� �����
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.layout.Region;

public class DemoSquareValue {
	
	Scene scene;
	
	HBox boxTask;
	VBox boxInputValue;
	HBox boxX;
	HBox boxResult;
	HBox boxButton;
	
	String stTask = "�������� �������: ������� ���������� ����������� ����� ���������,  \n"
			      + "�� ��������� ������� ������(X) = ��������� \n"
			      + "������� X \n"
			      + "������� ������ ������.\n";
	String stInputValue = "������� ��������";
	String stResult = "�����";
	String stButCalc = "������";
	String stButReset = "�����";
	
	Text txtHead;
	Label labelInput;
	Label labelResult;
	
	Button butCalc;
	Button butReset;	
		
	public DemoSquareValue(){	
		// ����������, � ��������� �� GridPane, ���� �� ��� 
		//��� ���������� �������� ������ 3� ������� �����
		//���� �������� ���������� ���� �� ��� ����� ������� ���������� ������, ��� ��� ���� �����������
		scene =  new Scene(new CalcWindow(),700,400 );			
	}
	
	class CalcWindow extends Region{
		
		double c = 0;
		TextField textInputX;
				
		public CalcWindow(){
			 
			 //�������� ������
			 boxTask = new HBox();			
			 TextArea txtAreaHead = new TextArea(stTask);
			 txtAreaHead.setFont(Font.font("Tahoma", FontWeight.NORMAL, 18));			 
			 txtAreaHead.setPrefHeight(130);
			 txtAreaHead.setPrefWidth(700);
			 boxTask.getChildren().add(txtAreaHead);
			 //���������� ����� �� ��������� ������� ����� ��������� ����� �������			 			 
			 
			 //��������� ������� ��������
			 boxInputValue = new VBox();
			 Label labelIV = new Label(stInputValue);
			 boxInputValue.getChildren().add(labelIV);
			 			  
			 //��������� ���������
		     updateResult(true);			 
			 
			 //������
			 boxButton = new HBox();
			 butCalc = new Button(stButCalc);
			 butReset = new Button(stButReset);
			 butCalc.setOnAction(e -> squareXValue(Double.valueOf(textInputX.getText())));
			 butReset.setOnAction(e -> clearTextField());
			 Label crutch = new Label("   ");//������� ��� ������� ����� ��������
			 boxButton.getChildren().addAll(butCalc, crutch, butReset);	     
			 
		     //� � ����//Y � ����
			 clearTextField();	
			 
		     updateWindow();//������� ����� � ���������� �� ��� ���� ������		     
		}
		
		   //��� ������ ������� ��� ��� ��������� �������� ���� ��������������� ��������� ���� ������
			@Override protected void layoutChildren() {
		        double w = getWidth();
		        double h = getHeight();		        
		       
		        //boxTask.setLayoutX(50); �� ��������
		        //boxTask.setMinHeight(80); �������� ����� �� �� ��������
		        		        
		        double taskH = boxTask.prefHeight(h);
		        double inputValueH = boxInputValue.prefHeight(h);
		        double XYH = boxX.prefHeight(h);
		        double ResultH = boxResult.prefHeight(h);
		        double buttonH = boxButton.prefHeight(h);
		        double buttonW = boxButton.prefWidth(w);
		        double indentH = 0;		        
		        layoutInArea(boxTask, 0 , indentH , w, taskH, 0, HPos.CENTER, VPos.CENTER); indentH+=taskH+30;
		        layoutInArea(boxInputValue, 10 , indentH , w, inputValueH, 0, HPos.CENTER, VPos.CENTER); indentH+=inputValueH+10;
		        layoutInArea(boxX, 10 , indentH , w, XYH, 0, HPos.CENTER, VPos.CENTER); indentH+=XYH+10;
		        layoutInArea(boxResult, 10 , indentH , w, ResultH, 0, HPos.CENTER, VPos.CENTER); 
		        layoutInArea(boxButton,  w-buttonW-10, h-buttonH-10, buttonW , buttonH , 0, HPos.CENTER, VPos.CENTER); 
		    }
		 
		   
		private void squareXValue(double x){
			c = Math.sqrt(x);
			updateResult(false);
			updateWindow();
		}
		
		private void updateResult(boolean clearKey){
			 boxResult = new HBox();
			 Label labelResult;
			 if (clearKey) labelResult = new Label(stResult + " = " );
			 else labelResult = new Label(stResult + " = " + c);
			 boxResult.getChildren().add(labelResult);			 
		}
		
		private void updateWindow(){
			getChildren().clear();
			getChildren().add(boxTask);
			getChildren().add(boxInputValue);
			getChildren().add(boxX);			
			getChildren().add(boxResult);
			getChildren().add(boxButton);
		}
		
		private void clearTextField(){
			//� � ����
			boxX = new HBox();
		    Label labelX = new Label("X=");			 
			boxX.getChildren().add(labelX);
			textInputX = new TextField();
			boxX.getChildren().add(textInputX);		 
			updateResult(true);
			updateWindow();
		}
		
	}	
	
	public Scene getScene(){
		return scene;
	}	
		
}

